<?php
    include_once("cabecalhoadmin.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
    $query = "select m.id,
    m.fk_id_campus,
    m.fk_id_tipopessoa, 
    m.fk_id_pessoa,
    m.nomepaciente, 
    m.providenciastomadas,
    to_char(m.ultimadatapresencial , 'DD/MM/YYYY') as ultimadatapresencial,
    to_char(m.dataprimeirossintomas , 'DD/MM/YYYY') as dataprimeirossintomas,
    to_char(m.datatestepcr , 'DD/MM/YYYY') as datatestepcr,
    to_char(m.dataconfirmacaoresultado , 'DD/MM/YYYY') as dataconfirmacaoresultado,
    to_char(m.datainserido , 'DD/MM/YYYY') as datainserido, tp.nome as nometipo,
    c.nome as nomecampus 
    from monitoramento m 
    join tipopessoa tp on tp.id=m.fk_id_tipopessoa
    join campus c on c.id = m.fk_id_campus
    order by datainserido desc";

    $resultado = $conexao->consultar($query);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php include_once "menuadmin.php";?>
        <div>
            <h1>Monitoramentos</h1>
        
            <table border="1">
                <tr>
                    <th>Código</th>
                    <th>Campus</th>
                    <th>Nome Paciente</th>
                    <th>Tipo Pessoa</th>
                    <th>Última Data Presencial</th>
                    <th>Primeiros Sintomas</th>
                    <th>Teste PCR</th>
                    <th>Confirmação Resultado</th>
                    <th>Data Inserido</th>
                    <th style="min-width:250px">Providencias</th>
                </tr>
                <tbody>
                    <?php
                        foreach($resultado as $linha){
                            echo "<tr>";
                            echo "<td>{$linha["id"]}</td>";
                            echo "<td>{$linha["nomecampus"]}</td>";
                            echo "<td>{$linha["nomepaciente"]}</td>";
                            echo "<td>{$linha["nometipo"]}</td>";
                            echo "<td>{$linha["ultimadatapresencial"]}</td>";
                            echo "<td>{$linha["dataprimeirossintomas"]}</td>";
                            echo "<td>{$linha["datatestepcr"]}</td>";
                            echo "<td>{$linha["dataconfirmacaoresultado"]}</td>";
                            echo "<td>{$linha["datainserido"]}</td>";
                            echo "<td>{$linha["providenciastomadas"]}</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
            
        </div>
    </body>
</html>
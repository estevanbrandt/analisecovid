<?php
    include_once("utilitario/conexao.php");
    
    class BuscarDados{
        function determinarAcao($acao){
            
           
            if($acao == "pesquisarPorMunicipio"){
                $this->pesquisarPorMunicipio();
            }
            else if($acao == "pesquisarPorMicroregiao"){
                $this->pesquisarPorMicroregiao();
            }
            else if($acao == "pesquisarPorMesoregiao"){
                $this->pesquisarPorMesoregiao();
            }
            else if($acao == "buscarMunicipios"){
                $this->buscarMunicipios();
            }
            else if($acao == "buscarMunicipiosPorMicro"){
                $this->buscarMunicipiosPorMicro();
            }
            else if($acao == "buscarMunicipiosPorMeso"){
                $this->buscarMunicipiosPorMeso();
            }
            else if($acao == "buscarMesoregiao"){
                $this->buscarMesoRegiao();
            }
            else if($acao == "buscarMicroregiao"){
                $this->buscarMicroregiao();
            }
        }

        function buscarMunicipios(){
            $conn = new Conexao();
            $conn->conectar();
            echo json_encode($conn->consultar("select * from municipio order by nome"));
        }

        function buscarMunicipiosPorMicro(){
            $conn = new Conexao();
            $conn->conectar();
            $parametros = Array(":micro" => $_POST["entrada"]);
            echo json_encode($conn->consultar("select * from municipio where fk_id_microregiao = :micro order by nome", $parametros));
        }

        function buscarMunicipiosPorMeso(){
            $conn = new Conexao();
            $conn->conectar();
            $parametros = Array(":meso" => $_POST["entrada"]);
            echo json_encode($conn->consultar("select m.* from municipio m 
            join microregiao mr on mr.id = m.fk_id_microregiao
            where mr.fk_id_mesoregiao = :meso order by m.nome", $parametros));
        }

        function buscarMesoregiao(){
            $conn = new Conexao();
            $conn->conectar();
            echo json_encode($conn->consultar("select * from mesoregiao order by nome"));
        }

        function buscarMicroregiao(){
            $conn = new Conexao();
            $conn->conectar();
            echo json_encode($conn->consultar("select * from microregiao order by nome"));
            
        }

        function pesquisarPorMicroregiao(){
            $conn = new Conexao();
            $conn->conectar();
            $parametros = Array(
                ":microregiao" => $_POST["origem"],
                ":datainicio" => $_POST["datainicio"],
                ":datafim" => $_POST["datafim"]
            );
            echo json_encode($conn->consultar("select mr.id, d.data ,sum(d.casos) as casos, sum(d.obitos) as obitos, sum(d.recuperados) as recuperados, sum(d.investigacao) as investigacao from dados d
            join municipio m on m.id = d.fk_id_municipio
            join microregiao mr on mr.id = m.fk_id_microregiao
            where mr.id = :microregiao and data between :datainicio and :datafim
            group by d.data, mr.id
            order by mr.id, d.data", $parametros ));
        }

        function pesquisarPorMesoregiao(){
            $conn = new Conexao();
            $conn->conectar();
            $parametros = Array(
                ":mesoregiao" => $_POST["origem"],
                ":datainicio" => $_POST["datainicio"],
                ":datafim" => $_POST["datafim"]
            );
            echo json_encode($conn->consultar("select mr.fk_id_mesoregiao, d.data ,sum(d.casos) as casos, sum(d.obitos) as obitos, sum(d.recuperados) as recuperados, sum(d.investigacao) as investigacao from dados d
            join municipio m on m.id = d.fk_id_municipio
            join microregiao mr on mr.id = m.fk_id_microregiao
            where mr.fk_id_mesoregiao = :mesoregiao and data between :datainicio and :datafim
            group by d.data, mr.fk_id_mesoregiao
            order by mr.fk_id_mesoregiao, d.data", $parametros ));
        }

        function pesquisarPorMunicipio(){
            $conn = new Conexao();
            $conn->conectar();
            $parametros = Array(
                ":municipio" => $_POST["origem"],
                ":datainicio" => $_POST["datainicio"],
                ":datafim" => $_POST["datafim"]
            );

            $query = "select * from dados where fk_id_municipio = :municipio and data between :datainicio and :datafim order by data";
            echo json_encode($conn->consultar($query, $parametros));
        }
    }

    $controle = new BuscarDados();
    $controle->determinarAcao($_POST["acao"]);


?>
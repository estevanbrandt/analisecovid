<!DOCTYPE html>
<html>
    <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script>
            
        </script>
    </head>
    <body>
    <?php
        include_once("utilitario/conexao.php");
        $conexao = new conexao();
        $conexao->conectar();

        $content = file_get_contents("https://www.saude.pr.gov.br/Pagina/Coronavirus-COVID-19");
        $total = preg_match_all("/title=\"Casos e Óbitos\">/" , $content);
        $needle = "title=\"Casos e Óbitos\">";
        $lastPos = 0;
        $positions = array();

        while (($lastPos = strpos($content, $needle, $lastPos))!== false) {
            $positions[] = $lastPos;
            $lastPos = $lastPos + strlen($needle);
            $iniciolink = strpos($content, "http", $lastPos);
            $finallink = strpos($content, ".csv", $iniciolink);
            $caminho = substr($content, $iniciolink, $finallink-$iniciolink+4);

            $data = substr($content, $lastPos-442, 10); // -442 funciona com algumas datas e -450 com outras
            
            $parametros = Array(
                ":caminho" => $caminho
            );

            $query = "select * from arquivoslidos where caminho = :caminho";

            $resultado = $conexao->consultar($query, $parametros);
            
            if(sizeof($resultado) == 0){
  
                $parametros["data"] = $data;
                //print_r($parametros);
                $query2 = "insert into arquivoslidos (caminho, data) values (:caminho, :data)";
                $conexao->executar($query2, $parametros);
                echo "$caminho inserido no banco de dados <br>";
            }
            
        }
        
    ?>
    </body>
</html>
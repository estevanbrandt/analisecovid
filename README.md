Este site serve para coletar e mostrar alguns dados do Paraná.

Uma vez a base de dados criada, você deve abrir a página de "carregardados.php" para poder buscar os links dos arquivos CSV.

Estes links serão gravados na tabela "arquivoslidos", e estarão indicando como "processados" = False, ou seja, ainda não foram lidos.

A tela de processardados.php pega todos os arquivos que ainda não foram lidos, e os le um a um. Esta tela tem duas questões:
 1 - Alguns arquivos csv antigos tem formatos diferentes. Não fiz um tratamento para eles. Quando isso ocorre, o sistema não consegue ler, e não grava nada. Por se tratar de informações referentes ao início da Pandemia, não dei muita atenção para este projeto.

 2 - Pode ser que a tela demore muito para processar, e a chamada trave. Basta abrir a página novamente para continuar. Este travamento é de servidor para servidor. O meu tinha uma restrição de 120 segundos. Como a primeira vez ele vai ler as informações de todos os meses dos últimos 2 anos, ele vai demorar. Uma vez lido, vai ser mais fácil atualizar poucos meses.
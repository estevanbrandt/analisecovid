<?php
    include_once("cabecalhoadmin.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
    $query = "select * from pessoa where id > -1 order by nome";

    $resultado = $conexao->consultar($query);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php include_once "menuadmin.php";?>
        <div>
            <h2>Usuários Cadastrados</h2>
            <?php
                if(isset($_GET["erro"])){
                    if($_GET["erro"] == "-1")
                        echo "<p>Senha alterada com sucesso!</p>";
                    else if($_GET["erro"] == "0")
                        echo "<p>Dados alterados com sucesso!</p>";
                    else if($_GET["erro"] == "1")
                    echo "<p>Ocorreu algum problema ao tentar atualizar. Tente novamente.</p>";
                    
                }
            ?>
            <a href="cadastrarnovousuario.php">Novo Usuário</a>
            <table border="1">
                <tr>
                    <th>Código</th>
                    <th>Nome</th>
                    <th>SIAPE</th>
                    <th>E-mail</th>
                    <th>Opção</th>
                </tr>
                <tbody>
                    <?php
                        foreach($resultado as $linha){
                            echo "<tr>";
                            echo "<td>{$linha["id"]}</td>";
                            echo "<td>{$linha["nome"]}</td>";
                            echo "<td>{$linha["siape"]}</td>";
                            echo "<td>{$linha["email"]}</td>";
                            echo "<td><a href='alterarusuario.php?pes={$linha["id"]}'>ALTERAR</a> </td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
            
        </div>
    </body>
</html>
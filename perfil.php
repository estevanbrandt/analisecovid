<?php
    include_once("cabecalhousuario.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h1>Usuário logado : <?php echo $_SESSION["nomepessoa"];?> </h1>
        <?php include_once "menuusuario.php";?>
        <div>
            <h3>Atualmente, responsável pelos campi:</h3>
            <ul>
                <?php
                    $query = "select c.* from campus c where c.fk_id_pessoa = :idpessoa";
                    $parametros = Array(":idpessoa" => $_SESSION["idpessoa"]);

                    $resultado = $conexao->consultar($query, $parametros);
                    if (count($resultado) == 0){
                        echo "<li>Sem Campus Registrados</li>";
                    }else{
                        foreach($resultado as $linha){
                            echo "<li>{$linha["id"]} - {$linha["nome"]}</li>";
                        }
                    }
                ?>
            </ul>
        </div>
        <div>
           <h2>Monitoramentos</h2>
                <?php
 
                    $query2 = "select m.id,
                    m.fk_id_campus,
                    m.fk_id_tipopessoa, 
                    m.fk_id_pessoa,
                    m.nomepaciente, 
                    m.providenciastomadas,
                    to_char(m.ultimadatapresencial , 'DD/MM/YYYY') as ultimadatapresencial,
                    to_char(m.dataprimeirossintomas , 'DD/MM/YYYY') as dataprimeirossintomas,
                    to_char(m.datatestepcr , 'DD/MM/YYYY') as datatestepcr,
                    to_char(m.dataconfirmacaoresultado , 'DD/MM/YYYY') as dataconfirmacaoresultado,
                    to_char(m.datainserido , 'DD/MM/YYYY') as datainserido, tp.nome as nometipo,
                    c.nome as nomecampus
                    
                    from monitoramento m 
                    join tipopessoa tp on tp.id=m.fk_id_tipopessoa
                    join campus c on c.id = m.fk_id_campus
                    where c.fk_id_pessoa = :idpessoa order by m.datainserido desc";

                    $parametros2 = Array(
                                         ":idpessoa" => $_SESSION["idpessoa"] );

                    $monitoramentos = $conexao->consultar($query2, $parametros2);
                ?>
            
            
            
            <table border="1">
                <tr>
                    <th>Código</th>
                    <th>Campus</th>
                    <th>Nome Paciente</th>
                    <th>Tipo Pessoa</th>
                    <th>Última Data Presencial</th>
                    <th>Primeiros Sintomas</th>
                    <th>Teste PCR</th>
                    <th>Confirmação Resultado</th>
                    <th>Data Inserido</th>
                    <th style="min-width:250px">Providencias</th>
                </tr>
                <tbody>
                    <?php
                        foreach($monitoramentos as $linha){
                            echo "<tr>";
                            echo "<td>{$linha["id"]}</td>";
                            echo "<td>{$linha["nomecampus"]}</td>";
                            echo "<td>{$linha["nomepaciente"]}</td>";
                            echo "<td>{$linha["nometipo"]}</td>";
                            echo "<td>{$linha["ultimadatapresencial"]}</td>";
                            echo "<td>{$linha["dataprimeirossintomas"]}</td>";
                            echo "<td>{$linha["datatestepcr"]}</td>";
                            echo "<td>{$linha["dataconfirmacaoresultado"]}</td>";
                            echo "<td>{$linha["datainserido"]}</td>";
                            echo "<td>{$linha["providenciastomadas"]}</td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
            
        </div>

    </body>
</html>
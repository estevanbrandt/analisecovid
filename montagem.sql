create table mesoregiao(
    id bigint primary key,
    nome varchar(300)
);

create table microregiao(
    id bigint primary key,
    nome varchar(300),
    fk_id_mesoregiao bigint,
    foreign key (fk_id_mesoregiao) references mesoregiao (id)
);

create table municipio (
    id bigint primary key,
    nome varchar(300),
    fk_id_microregiao bigint,
    foreign key (fk_id_microregiao) references microregiao (id)
);

create table arquivoslidos(
    id bigserial primary key,
    dataimportado timestamp default current_timestamp,
    caminho varchar(500),
    processado boolean default false,
    data date
);

create table dados(
    id bigserial primary key,
    data date,
    fk_id_municipio bigint not null,
    fk_id_arquivoorigem bigint not null,
    casos bigint default 0,
    obitos bigint default 0,
    recuperados bigint default 0,
    investigacao bigint default 0,
    foreign key (fk_id_municipio) references municipio (id),
    foreign key (fk_id_arquivoorigem) references arquivoslidos (id)
);

create table pessoa(
    id bigserial primary key,
    siape varchar(20),
    nome varchar(200

u	nome varchar(200),000cm,v.bx;cx..v.............................
    fk_id_pessoa bigint,
    foreign key (fk_id_pessoa) references pessoa (id)
);

create table tipopessoa(
	id int primary key,
	nome varchar (200)
);

insert into tipopessoa values (1, 'Docente') , (2 , 'Técnico Administrativo') , (3 , 'Estudante'), (4, 'Terceirizado') , (5, 'Estagiário');

create table monitoramento(
	id bigserial primary key,
	fk_id_campus int not null,
	fk_id_tipopessoa int not null,
    fk_id_pessoa bigint not null,
	nomepaciente varchar(200),
	ultimadatapresencial date,
	dataprimeirossintomas date,
	datatestepcr date,
	dataconfirmacaoresultado date,
	providenciastomadas text,
    datainserido timestamp default current_timestamp,
	foreign key (fk_id_tipopessoa) references tipopessoa (id),
	foreign key (fk_id_campus) references campus (id),
    foreign key (fk_id_pessoa) references pessoa (id)
);

insert into pessoa values (-1, 'admin', 'admin', 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3');

insert into campus (id, nome) values (1 , 'Arapongas'), 
                                     (2 , 'Assis Chateaubriand'),
                                     (3 , 'Astorga'),
                                     (4 , 'Barracão'),
                                     (5 , 'Campo Largo'),
                                     (6 , 'Capanema'),
                                     (7 , 'Cascavel'),
                                     (8 , 'Colombo'),
                                     (9 , 'Coronel Vivida'),
                                     (10 , 'Curitiba'),
                                     (11 , 'Foz do Iguaçu'),
                                     (12 , 'Goioerê'),
                                     (13 , 'Irati'),
                                     (14 , 'Ivaiporã'),
                                     (15 , 'Jacarezinho'),
                                     (16 , 'Jaguariaíva'),
                                     (17 , 'Londrina'),
                                     (18 , 'Palmas'),
                                     (19 , 'Paranaguá'),
                                     (20 , 'Paranavaí'),
                                     (21 , 'Pinhais'),
                                     (22 , 'Pitanga'),
                                     (23 , 'Quedas do Iguaçu'),
                                     (24 , 'Telêmaco Borba'),
                                     (25 , 'Umuarama'),
                                     (26 , 'União da Vitória'),
                                     (27 , 'Reitoria');

insert into campus values (-1 , 'Central' , -1);

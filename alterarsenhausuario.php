<?php
    include_once("cabecalhoadmin.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
    
    if(count($_POST) == 0)
        header("location: usuarioscadastrados.php?erro=1");

    $query = "update pessoa set senha = :senha where id = :id";
    $parametros = Array (":senha" => md5($_POST["senha"]),
                         ":id" => $_POST["id"]);
    $conexao->executar($query, $parametros);
    header("location: usuarioscadastrados.php?erro=-1");

?>
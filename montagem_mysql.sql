create table mesoregiao(
    id bigint primary key,
    nome varchar(300)
);

create table microregiao(
    id bigint primary key,
    nome varchar(300),
    fk_id_mesoregiao bigint,
    foreign key (fk_id_mesoregiao) references mesoregiao (id)
);

create table municipio (
    id bigint primary key,
    nome varchar(300),
    fk_id_microregiao bigint,
    foreign key (fk_id_microregiao) references microregiao (id)
);

create table arquivoslidos(
    id bigint auto_increment primary key,
    dataimportado timestamp default current_timestamp,
    caminho varchar(500),
    processado boolean default false,
    data date
);

create table dados(
    id bigint auto_increment primary key,
    data date,
    fk_id_municipio bigint not null,
    fk_id_arquivoorigem bigint not null,
    casos bigint default 0,
    obitos bigint default 0,
    recuperados bigint default 0,
    investigacao bigint default 0,
    foreign key (fk_id_municipio) references municipio (id),
    foreign key (fk_id_arquivoorigem) references arquivoslidos (id)
);

create table campus (
	id int primary key,
	nome varchar(200)
);

create table tipopessoa(
	id int primary key,
	nome varchar (200)
);

insert into tipopessoa values (1, 'Docente') , (2 , 'Técnico Administrativo') , (3 , 'Estudante'), (4, 'Terceirizado') , (5, 'Estagiário');

create table monitoramento(
	id bigint auto_increment primary key,
	fk_id_campus int not null,
	fk_id_tipopessoa int not null,
	nomeresponsavel varchar(200),
	nomepaciente varchar(200),
	ultimadatapresencial date,
	dataprimeirossintomas date,
	datatestepcr date,
	dataconfirmacaoresultado date,
	providenciastomadas text,
	foreign key (fk_id_tipopessoa) references tipopessoa (id),
	foreign key (fk_id_campus) references campus (id)
);
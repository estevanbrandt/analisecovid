<?php
    include_once("cabecalhoadmin.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
    
    if(count($_POST) == 0)
        header("location: cadastrarnovousuario.php");

    $query = "insert into pessoa (nome, siape, email, senha) values (:nome, :siape, :email, :senha)";
    $parametros = Array (":nome" => $_POST["nome"],
                         ":siape" => $_POST["siape"],
                         ":email" => $_POST["email"],
                         ":senha" => md5($_POST["senha"]));
    $conexao->executar($query, $parametros);
    header("location: usuarioscadastrados.php");

?>
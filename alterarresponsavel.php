<?php
    include_once("cabecalhoadmin.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php include_once "menuadmin.php";?>
        <div>
            <h2>Responsável Atual pelo Campus</h2>
            <?php
                $query = "select c.*, coalesce(p.nome, 'Sem Responsável') as nomepessoa, coalesce(p.email, '--') as email from campus c 
                left join pessoa p on p.id = c.fk_id_pessoa where c.id = :id ";
                $parametros = Array(":id"=> $_GET["cmp"]);

                $resultado = $conexao->consultar($query, $parametros);

                echo "<p>{$resultado[0]["nomepessoa"]}</p>";
            ?>
            <form action="escolhernovoresponsavel.php" method="post">
                <input type="hidden" name="idcampus" value="<?php echo $_GET["cmp"]; ?>">
                <h3>Escolha um Novo Responsável</h3>
                <select name="idpessoa">
                    <?php 
                        $query = "select * from pessoa order by nome";

                        $resultado = $conexao->consultar($query);
                        foreach ($resultado as $linha){
                            echo "<option value='{$linha["id"]}'>{$linha["nome"]}</option>";
                        }
                    ?>
                </select><br>
                <input type="submit" value="Atualizar Responsável">
            </form>
            
        </div>
    </body>
</html>
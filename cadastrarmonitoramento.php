<?php
    include_once("cabecalhousuario.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php include_once "menuusuario.php";?>
        <h2>Cadastro de Novo Monitoramento</h2>
        <form action="gravardadosmonitoramento.php" method="POST">
            <input type="hidden" name="cmp" value="<?php echo $_GET["cmp"]; ?>" >
            Nome Paciente: <input type="text" name="nomepaciente" maxlength="200"><br>
            Tipo: <select name="fk_id_tipopessoa">
                <?php
                    $query = "select * from tipopessoa order by id";
                    $resultado = $conexao->consultar($query);
                    foreach ($resultado as $linha){
                        echo "<option value='{$linha["id"]}'>{$linha["nome"]}</option>";
                    }
                ?>
            </select> <br>
            Última Data Presencial: <input type="text" name="ultimadatapresencial" maxlength="30"><br>
            Primeiros Sintomas: <input type="text" name="primeirossintomas" maxlength="30"><br>
            Teste PCR: <input type="text" name="testepcr" maxlength="30"><br>        
            Confirmação do Resultado: <input type="text" name="confirmacaoresultado" maxlength="30"><br>
            Providências Tomadas:
            <textarea name="providencias"></textarea><br>
            <input type="submit">
        </form>
    </body>
</html>
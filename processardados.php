<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php
            include_once("utilitario/conexao.php");
            $conexao = new conexao();
            $conexao->conectar();

            $arquivos = $conexao->consultar("select * from arquivoslidos where processado is false");
            foreach($arquivos as $arquivolido){
                //print_r($arquivolido);
                $content = file_get_contents($arquivolido["caminho"]);
                $dados = (explode(";",$content));
    
                $primeira = explode("\n",$dados[7]);
    
    
                $codigo = $primeira[1];
                echo "processando arquivo ".$arquivolido["id"];
                for($i=8; $i<count($dados); $i=$i+7){
                    
                    //echo "codigo : $codigo <br>";
                    //echo "casos : ".$dados[$i+3]."<br>";
                    //echo "obitos : ".$dados[$i+4]."<br>";
                    //echo "Recuperados : ".$dados[$i+5]."<br>";
                    $linha = explode("\n",$dados[$i+6]);
                    //echo "Investigação : ".$linha[0]."<br>";
                    $parametros = Array(
                        ":fk_id_municipio" => $codigo,
                        ":casos" => $dados[$i+3],
                        ":obitos" => $dados[$i+4],
                        ":recuperados" => $dados[$i+5],
                        ":investigacao" => $linha[0],
                        ":fk_id_arquivoorigem" => $arquivolido["id"],
                        ":data" => $arquivolido["data"]
                    );
    
                    $query = "insert into dados (data, fk_id_municipio, fk_id_arquivoorigem ,casos, obitos, recuperados, investigacao) 
                    values (:data , :fk_id_municipio, :fk_id_arquivoorigem, :casos, :obitos, :recuperados, :investigacao) ";
    
                    $conexao->executar($query, $parametros);
                    $codigo = $linha[1];
                }

                $param = Array(
                    ":id" => $arquivolido["id"]
                );

                $conexao->executar("update arquivoslidos set processado = true where id = :id", $param);
                echo "terminou<br>";
            }

           
            //print_r($dados);
            

        ?>
    </body>
</html>
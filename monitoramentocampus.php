<?php
    include_once("cabecalhousuario.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
    
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php include_once "menuusuario.php";?>
        <?php
            if(!isset($_GET["cmp"])){
                $query = "select c.* from campus c where c.fk_id_pessoa = :idpessoa order by c.nome";
                $parametros = Array(":idpessoa" => $_SESSION["idpessoa"]);
                $resultadocampus = $conexao->consultar($query, $parametros);
                ?>
            <div>
                <h1>Selecione um Campus </h1>
                <form action="monitoramentocampus.php" method="GET">
                    <select name="cmp">
                        <?php
                            foreach($resultadocampus as $linha){
                                echo "<option value='{$linha["id"]}'>{$linha["nome"]} </option>";
                            }
                        ?>
                    </select>
                    <input type="submit" value="Carregar">

                </form>
            </div>

            <?php
            }
            else{
        ?>
        
        <div>
            <h1>Monitoramento do Campus 
                <?php
                    $query = "select * from campus where id = :id";
                    $parametros = Array (":id" => $_GET["cmp"]);

                    $resultadocampus = $conexao->consultar($query, $parametros);

                    echo $resultadocampus[0]["nome"];

                    $query2 = "select m.id,
                    m.fk_id_campus,
                    m.fk_id_tipopessoa, 
                    m.fk_id_pessoa,
                    m.nomepaciente, 
                    m.providenciastomadas,
                    to_char(m.ultimadatapresencial , 'DD/MM/YYYY') as ultimadatapresencial,
                    to_char(m.dataprimeirossintomas , 'DD/MM/YYYY') as dataprimeirossintomas,
                    to_char(m.datatestepcr , 'DD/MM/YYYY') as datatestepcr,
                    to_char(m.dataconfirmacaoresultado , 'DD/MM/YYYY') as dataconfirmacaoresultado,
                    to_char(m.datainserido , 'DD/MM/YYYY') as datainserido, tp.nome as nometipo from monitoramento m 
                    join tipopessoa tp on tp.id=m.fk_id_tipopessoa
                    join campus c on c.id = m.fk_id_campus
                    where m.fk_id_campus = :idcampus and c.fk_id_pessoa = :idpessoa
                    order by m.datainserido desc";

                    $parametros2 = Array(":idcampus" => $_GET["cmp"],
                                         ":idpessoa" => $_SESSION["idpessoa"] );

                    $monitoramentos = $conexao->consultar($query2, $parametros2);
                ?>
            </h1>
            <a href="monitoramentocampus.php">Escolher outro Campus </a>
            <br>
            <?php
                echo "<a href='cadastrarmonitoramento.php?cmp={$_GET["cmp"]}'>Cadastrar novo Monitoramento</a>"; 
            ?>
            <table border="1">
                <tr>
                    <th>Código</th>
                    <th>Nome Paciente</th>
                    <th>Tipo Pessoa</th>
                    <th>Última Data Presencial</th>
                    <th>Primeiros Sintomas</th>
                    <th>Teste PCR</th>
                    <th>Confirmação Resultado</th>
                    <th>Data Inserido</th>
                    <th style="min-width:250px">Providencias</th>
                    <th>Opção</th>
                </tr>
                <tbody>
                    <?php
                        foreach($monitoramentos as $linha){
                            echo "<tr>";
                            echo "<td>{$linha["id"]}</td>";
                            echo "<td>{$linha["nomepaciente"]}</td>";
                            echo "<td>{$linha["nometipo"]}</td>";
                            echo "<td>{$linha["ultimadatapresencial"]}</td>";
                            echo "<td>{$linha["dataprimeirossintomas"]}</td>";
                            echo "<td>{$linha["datatestepcr"]}</td>";
                            echo "<td>{$linha["dataconfirmacaoresultado"]}</td>";
                            echo "<td>{$linha["datainserido"]}</td>";
                            echo "<td>{$linha["providenciastomadas"]}</td>";
                            echo "<td><a href='detalharmonitoramentousuario.php?monit={$linha["id"]}'>Detalhar</a> </td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
            
        </div>
        <?php 
        }
        ?>
    </body>
</html>
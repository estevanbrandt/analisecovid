<?php
    include_once("cabecalhoadmin.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
    $query = "select c.*, coalesce(p.nome, 'Sem Responsável') as nomepessoa, coalesce(p.email, '--') as email from campus c 
    left join pessoa p on p.id = c.fk_id_pessoa where c.id >-1 order by c.id";

    $resultado = $conexao->consultar($query);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php include_once "menuadmin.php";?>
        <div>
            <h2>Campi Cadastrados</h2>
        
            <table border="1">
                <tr>
                    <th>Código</th>
                    <th>Campus</th>
                    <th>Responsável</th>
                    <th>E-mail</th>
                    <th>Opção</th>
                </tr>
                <tbody>
                    <?php
                        foreach($resultado as $linha){
                            echo "<tr>";
                            echo "<td>{$linha["id"]}</td>";
                            echo "<td>{$linha["nome"]}</td>";
                            echo "<td>{$linha["nomepessoa"]}</td>";
                            echo "<td>{$linha["email"]}</td>";
                            echo "<td><a href='alterarresponsavel.php?cmp={$linha["id"]}'>Alterar Responsável</a> </td>";
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>
            
        </div>
    </body>
</html>
<?php
    include_once("cabecalhousuario.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
    $query = "select * from pessoa where id = :id";
    $parametros = Array (":id" => $_SESSION["idpessoa"]);
    $resultado = $conexao->consultar($query, $parametros);
    
    $nome = "";
    $siape = "";
    $email = "";

    if(count($resultado)>0){
        $nome = $resultado[0]["nome"];
        $siape = $resultado[0]["siape"];
        $email = $resultado[0]["email"];
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php include_once "menuusuario.php";?>
        <div>
            <h2>Meus Dados</h2>
            <?php
                if(isset($_GET["msg"])){
                    if($_GET["msg"] == "1")
                        echo "<p>Dados Atualizados com Sucesso!</p>";
                    else if($_GET["msg"] == "2")
                        echo "<p>Senha Atualizada com Sucesso!</p>";
                }
            ?>
            <form action="atualizardadosperfilusuario.php" method="POST">
                Nome: <input type="text" name="nome" value="<?php echo $nome; ?>"><br>
                E-mail: <input type="email" name="email" value="<?php echo $email; ?>"><br>
                SIAPE: <input type="text" name="siape" value="<?php echo $siape; ?>"><br>
                <input type="submit" value="Atualizar Dados">
            </form>
            <hr>
            <form action="atualizarsenhausuario.php" method="POST">
                Nova Senha: <input type="password" name="senha">
                <input type="submit" value="Atualizar Senha">
            </form>
        </div>
    </body>
</html>
<?php
     include_once("cabecalhoadmin.php");
     include_once("utilitario/conexao.php");
     $conexao = new conexao();
     $conexao->conectar();

     if(count($_POST[""]) == 0){
         header("location: meusdadosusuario.php");
     }

     $query = "update pessoa set senha = :senha where id = :id";
     $parametros = Array(
            ":senha" => md5($_POST["senha"]),
            ":id" => $_SESSION["idpessoa"]);
     $conexao->executar($query, $parametros);
     header("location: meusdadosusuario.php?msg=2");
?>
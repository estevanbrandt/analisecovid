<?php
     include_once("cabecalhoadmin.php");
     include_once("utilitario/conexao.php");
     $conexao = new conexao();
     $conexao->conectar();

     if(count($_POST[""]) == 0){
         header("location: meusdadosadmin.php");
     }

     $query = "update pessoa set nome = :nome, siape = :siape, email = :email where id = -1";
     $parametros = Array(
            ":nome" => $_POST["nome"],
            ":email" => $_POST["email"],
            ":siape" => $_POST["siape"]);
     $conexao->executar($query, $parametros);
     header("location: meusdadosadmin.php?msg=1");
?>
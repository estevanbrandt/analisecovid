<?php
    include_once("cabecalhoadmin.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
    
    if(count($_POST) == 0)
        header("location: usuarioscadastrados.php?erro=1");

    $query = "update pessoa set nome = :nome, siape = :siape, email = :email where id = :id";
    $parametros = Array (":nome" => $_POST["nome"],
                         ":siape" => $_POST["siape"],
                         ":email" => $_POST["email"],
                         ":id" => $_POST["id"]);
    $conexao->executar($query, $parametros);
    header("location: usuarioscadastrados.php?erro=0");

?>
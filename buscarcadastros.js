
function trocar(origem){
    return origem.substring(6,10) + '-' + origem.substring(3,5) +"-"+ origem.substring(0,2);
}

function voltar(origem){
    return  origem.substring(8,10)+'/'+origem.substring(5,7)+'/'+origem.substring(0,4);
}

function buscarMesoregiao(elemento){
    chamadaAjax(elemento, "buscarMesoregiao");
}

function buscarMicroregiao(elemento){
    chamadaAjax(elemento, "buscarMicroregiao");
}

function buscarMunicipios(elemento){
    chamadaAjax(elemento, "buscarMunicipios");
}

function chamadaAjax(elemento, acaoDesejada){
    $.ajax({
        url: "buscardados.php",
        type: "POST",
        data: {
            acao: acaoDesejada
        },
        success: function(result){
            resultado = JSON.parse(result);
            
            $(elemento).html(montarOpcoes(resultado));
            $(elemento).trigger("change");
        }
    });
}

function montarOpcoes(dados){
    let opcoes = "";
    for(i=0; i< dados.length; i++){
        opcoes += "<option value='"+dados[i].id+"'>"+dados[i].nome+"</option>";
    }
    return opcoes; 
}

function buscarDados(elementoEntrada, elementoSaida, acaoDesejada){
    $.ajax({
        url: "buscardados.php",
        type: "POST",
        data:{
            acao : acaoDesejada,
            origem: $(elementoEntrada).val(),
            datainicio: trocar($("#datainicio").val()),
            datafim: trocar($("#datafim").val())
        },
        success: function(result){
            dados = JSON.parse(result);
            montarTabela(elementoSaida, dados);
            
        }
    });
}

function montarTabela(elemento, dados){
    let valores = [];
    var linhas = "";
    anterior = dados[0].casos;
    inicioCasos = dados[0].casos;
    inicioObitos = dados[0].obitos;
    inicioRecuperados = dados[0].recuperados;
    ultimo = dados.length - 1;
    finalCasos = dados[ultimo].casos;
    finalObitos = dados[ultimo].obitos;
    finalRecuperados = dados[ultimo].recuperados;

    $("#novosCasos").html(finalCasos - inicioCasos);
    $("#novosObitos").html(finalObitos - inicioObitos);
    $("#novosRecuperados").html(finalRecuperados - inicioRecuperados);
    for(i=0;i < dados.length; i++){
        valores[i] = [voltar(dados[i].data), dados[i].casos, dados[i].obitos/*, dados[i].investigacao, dados[i].recuperados*/];
        var linha = "<tr>";
        linha += "<td>"+voltar(dados[i].data)+"</td>";
        linha += "<td>"+dados[i].casos+"</td>";
        linha += "<td>"+(dados[i].casos - anterior)+"</td>";
        anterior = dados[i].casos;
        linha += "<td>"+dados[i].obitos+"</td>";
        linha += "<td>"+dados[i].recuperados+"</td>";
        linha += "<td>"+dados[i].investigacao+"</td>";
        linha += "</tr>";
        linhas += linha;
    }
    $(elemento).html(linhas);
}

function buscarMunicipiosPorMeso(elementoSaida, elementoEntrada){
    chamadaAjaxMunicipiosPorRegiao(elementoSaida, $(elementoEntrada).val(), "buscarMunicipiosPorMeso");
}


function buscarMunicipiosPorMicro(elementoSaida, elementoEntrada){
    chamadaAjaxMunicipiosPorRegiao(elementoSaida, $(elementoEntrada).val(), "buscarMunicipiosPorMicro");
}

function chamadaAjaxMunicipiosPorRegiao(elemento, valorEntrada, acaoDesejada){
    $.ajax({
        url: "buscardados.php",
        type: "POST",
        data:{
            acao : acaoDesejada,
            entrada : valorEntrada
        },
        success: function(result){
            montarListaMunicipios(elemento, JSON.parse(result));
        }
    });
}

function montarListaMunicipios(elemento, dados){
    let nomes = "";
    for (i=0; i< dados.length; i++){
        nomes += "<li>"+dados[i].nome+"</li>";
    }
    $(elemento).html(nomes);
}
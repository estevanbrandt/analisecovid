<?php
    include_once("cabecalhoadmin.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();

    if(!isset($_GET["pes"]))
        header("location: usuarioscadastrados.php");

    $query = "select * from pessoa where id = :id";
    $parametros = Array(":id"=>$_GET["pes"]);
    $resultado = $conexao->consultar($query, $parametros);
    $linha = $resultado[0];

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php include_once "menuadmin.php";?>
        <h2>Cadastro de Novo Usuário</h2>
        <form action="alterardadosusuario.php" method="POST">
            <input type="hidden" name="id" value="<?php echo $_GET["pes"]; ?>">
            Nome: <input type="text" name="nome" maxlength="200" value="<?php echo $linha["nome"]; ?>"><br>
            Email: <input type="email" name="email" maxlength="200" value="<?php echo $linha["email"]; ?>"><br>
            SIAPE: <input type="text" name="siape" maxlength="30" value="<?php echo $linha["siape"]; ?>"><br>
            <input type="submit" value="Atualizar Dados">
        </form>    
        <form action="alterarsenhausuario.php" method="POST">
            <hr>
            <input type="hidden" name="id" value="<?php echo $_GET["pes"]; ?>">
            Senha: <input type="text" name="senha"> <br> 
            <input type="submit" value="Atualizar Senha">
        </form>
        
    </body>
</html>
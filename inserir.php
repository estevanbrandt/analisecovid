<?php
    include_once("utilitario/conexao.php");
    //var_dump($_POST);
    if(isset($_POST["dados"])){
        $opcao = $_POST["tabela"];
        $dados = json_decode($_POST["dados"]);
        //var_dump($dados);
        $conexao = new conexao();
        $conexao->conectar();
        if($opcao == "1"){
            foreach($dados as $meso){
                $parametros = Array(
                    ":id" => $meso->id,
                    ":nome" => $meso->nome
                );

                $query = "insert into mesoregiao values (:id, :nome)";
                $conexao->executar($query, $parametros);
            }
        }else if ($opcao == "2"){
            foreach ($dados as $micro){
                $parametros = Array(
                    ":id"=> $micro->id,
                    ":nome"=> $micro->nome,
                    ":meso"=> $micro->mesorregiao->id
                );
                $query = "insert into microregiao values (:id, :nome, :meso)";
                $conexao->executar($query, $parametros);
            }

        }else if ($opcao == "3"){
            foreach ($dados as $muni){
                $parametros = Array(
                    ":id"=> $muni->id,
                    ":nome"=> $muni->nome,
                    ":micro"=> $muni->microrregiao->id
                );
                $query = "insert into municipio values (:id, :nome, :micro)";
                $conexao->executar($query, $parametros);
            }
        }
    }
?>
<!DOCTYPE html>
<html>
    <head></head>
    <body>
        <form action="inserir.php" method="POST">
            <textarea name="dados"></textarea>
            <select name="tabela">
                <option value="1">Meso Regiao</option>
                <option value="2">Micro Regiao</option>
                <option value="3">Municipio</option>
            </select>
            <input type="submit">
        </form>
    </body>
</html>
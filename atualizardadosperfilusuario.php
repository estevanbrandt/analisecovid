<?php
     include_once("cabecalhousuario.php");
     include_once("utilitario/conexao.php");
     $conexao = new conexao();
     $conexao->conectar();

     if(count($_POST[""]) == 0){
         header("location: meusdadosusuario.php");
     }

     $query = "update pessoa set nome = :nome, siape = :siape, email = :email where id = :id";
     $parametros = Array(
            ":nome" => $_POST["nome"],
            ":email" => $_POST["email"],
            ":siape" => $_POST["siape"],
            ":id" => $_SESSION["idpessoa"]);
     $conexao->executar($query, $parametros);
     header("location: meusdadosusuario.php?msg=1");
?>
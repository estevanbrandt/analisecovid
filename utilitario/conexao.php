<?php
   if(session_id() == '' || !isset($_SESSION) || session_status() === PHP_SESSION_NONE) {
      // session isn't started
      session_start();
  }
	date_default_timezone_set("America/Sao_Paulo");

   class Conexao
   {
   		public static $conn;

   		public function conectar()
   		{
            try{
               self::$conn = new PDO("pgsql:dbname=analisecovid;port=5432;host=localhost", "postgres", "postgres");
            }
   			catch(PDOException $e){
       			echo $e->getMessage();
            }
   		}

         public function beginTransacao()
         {
            if (self::$conn == null)
               self::conectar();
            self::$conn->beginTransaction();
         }

         public function commitTransacao()
         {
            self::$conn->commit();
         }

         public function rollbackTransacao()
         {
            self::$conn->rollBack();
         }

         public function executarComRetornoId(){
            $numparametros = func_num_args();

            if (self::$conn == null)
               self::conectar();

            $query = func_get_arg(0)." returning *";

            $stmt = self::$conn->prepare($query);
            if($numparametros == 2){
               $parametros = func_get_arg(1);
               if (!empty($parametros))
                  foreach ($parametros as $nome => $valor){
                     $stmt->bindValue($nome, $valor);
                  }
            }

            $stmt->execute();
	    
            $retorno = $stmt->fetchAll(PDO::FETCH_NUM);
            
            return $retorno[0][0];
        }

   		public function executar()
   		{
            $numparametros = func_num_args();

            if (self::$conn == null)
               self::conectar();

            $query = func_get_arg(0);

            $stmt = self::$conn->prepare($query);
            if($numparametros == 2){
               $parametros = func_get_arg(1);
               if (!empty($parametros))
                  foreach ($parametros as $nome => $valor){
                     $stmt->bindValue($nome, $valor);
                  }
            }

            return $stmt->execute();
   		}

   		public function consultar()
   		{
            $numparametros = func_num_args();

            if (self::$conn == null)
               self::conectar();

            $query = func_get_arg(0);

            $stmt = self::$conn->prepare($query);
            if($numparametros == 2){
               $parametros = func_get_arg(1);
               if (!empty($parametros))
                  foreach ($parametros as $nome => $valor){
                     $stmt->bindValue($nome, $valor);
                  }
            }

            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
         }
         
         public function consultarObjeto()
   		{
            $numparametros = func_num_args();

            if (self::$conn == null)
               self::conectar();

            $query = func_get_arg(0);
            $stmt = self::$conn->prepare($query);
            if($numparametros == 2){
               $parametros = func_get_arg(1);
               if (!empty($parametros))
                  foreach ($parametros as $nome => $valor){
                     $stmt->bindValue($nome, $valor);
                  }
            }

            $stmt->execute();
   			return $stmt->fetchAll(PDO::FETCH_CLASS);
         }
   }
?>

<?php
    include_once("utilitario/conexao.php");
    $conexao = new Conexao();
    $conexao->conectar();
?>
<html>

  <head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="buscarcadastros.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        buscarMicroregiao("#micro");
        $("#btnCarregar").click(()=>{
           buscarDados("#micro", "#corpoTabela", "pesquisarPorMicroregiao");
           
        });

        $("#micro").change(function(){
            buscarMunicipiosPorMicro("#nomeMunicipios", "#micro");
        });
      });  
    </script>
  </head>

  <body>
    <h1>Pesquisar Por Micro Região</h1>
    <a href="index.php">Voltar</a><br>
    <!--Div that will hold the pie chart-->
    <select id="micro">
    </select>
    <div>
        <h4>Municípios contidos neste região:</h4>
        <ul id="nomeMunicipios">
        </ul>
    </div>
    <br>
    Data Início: <input type="text" id="datainicio" value="01/05/2021"> <br>
    Data Final: <input type="text" id="datafim" value="30/05/2021"> <br>
    <button id="btnCarregar" >Carregar </button>
    <div id="chart_div"></div>
    <div>
        <h3>Neste período tivemos</h3>
        <p><span id="novosCasos">0</span><strong> Novos Casos</strong></p>
        <p><span id="novosObitos">0</span><strong> Óbitos</strong></p>
        <p><span id="novosRecuperados">0</span><strong> Recuperados</strong></p>
        <table border = "1">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Casos Confirmados </th>
                    <th>Novos Casos</th>
                    <th>Óbitos </th>
                    <th>Recuperados </th>
                    <th>Investigação </th>
                </tr>
            </thead>
            <tbody id="corpoTabela">

            </tbody>
        </table>
    </div>
  </body>
</html>
<?php
    session_start();
    if(count($_POST) == 0){
        header ("location: index.php");
    }

    $email = $_POST["email"];
    $senha = md5($_POST["senha"]);

    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();

    $query  = "select * from pessoa where email = :email and senha = :senha";
    $parametros = Array(":email" => $email, ":senha" => $senha);

    $resultado = $conexao->consultar($query, $parametros);
    
    if(count($resultado) == 0){
        header("location: index.php?erro=1");
    }else{
        
        $_SESSION["idpessoa"] = $resultado[0]["id"];
        $_SESSION["nomepessoa"] = $resultado[0]["nome"];
        if($_SESSION["idpessoa"] == "-1")
            header("location: perfiladm.php");
        else 
            header("location: perfil.php");
    }

?>
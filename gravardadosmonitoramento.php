<?php
    include_once("cabecalhousuario.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
    
    if(count($_POST) == 0)
        header("location: monitoramentocampus.php");

    $query = "insert into monitoramento 
        (fk_id_campus, fk_id_pessoa, fk_id_tipopessoa, nomepaciente, ultimadatapresencial, dataprimeirossintomas, datatestepcr, dataconfirmacaoresultado, providenciastomadas) values 
        (:fk_id_campus, :fk_id_pessoa, :fk_id_tipopessoa, :nomepaciente, :ultimadatapresencial, :dataprimeirossintomas, :datatestepcr, :dataconfirmacaoresultado, :providenciastomadas)";
    $parametros = Array (":fk_id_campus" => $_POST["cmp"],
                         ":fk_id_tipopessoa" => $_POST["fk_id_tipopessoa"],
                         ":fk_id_pessoa" => $_SESSION["idpessoa"],
                         ":nomepaciente" => $_POST["nomepaciente"],
                         ":ultimadatapresencial" => $_POST["ultimadatapresencial"],
                         ":dataprimeirossintomas" =>$_POST["primeirossintomas"],
                         ":datatestepcr" => $_POST["testepcr"],
                         ":dataconfirmacaoresultado" => $_POST["confirmacaoresultado"],
                         ":providenciastomadas" => $_POST["providencias"]);
    //print_r($parametros);
    $conexao->executar($query, $parametros);
    header("location: monitoramentocampus.php?cmp={$_POST["cmp"]}");

?>
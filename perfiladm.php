<?php
    include_once("cabecalhoadmin.php");
    include_once("utilitario/conexao.php");
    $conexao = new conexao();
    $conexao->conectar();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h1>Usuário logado : <?php echo $_SESSION["nomepessoa"];?> </h1>
        <?php include_once "menuadmin.php";?>
        <div>
            <h3>Dados Gerais</h3>
            <p>Totais por Tipo de Pessoa </p>
            <ul>
            <?php
                $query = "select tp.nome, count(m.id) as total from monitoramento m
                join tipopessoa tp on tp.id = m.fk_id_tipopessoa
                group by tp.nome
                order by tp.nome";

                $resultado = $conexao->consultar($query);
                foreach($resultado as $linha){
                    echo "<li>{$linha["nome"]} : {$linha["total"]}</li>";
                }
            ?>
            </ul>
        </div>
        <div>
            <h3>Dados Por Campus</h3>
            <table border="1">
                <thead>
                    <tr>
                        <th>Campus</th>
                        <th>Docentes</th>
                        <th>Técnicos</th>
                        <th>Estudantes</th>
                        <th>Terceirizados</th>
                        <th>Estagiários</th>
                        <th>Total</th>
                        <th>Detalhar</th>
                    </tr>
                </thead>
                <tbody>
            <?php
                $query = "select c.id, c.nome,
                coalesce((select count(m.id) as total from monitoramento m where m.fk_id_tipopessoa = 1 and m.fk_id_campus = c.id),0) as totdocentes,
                coalesce((select count(m.id) as total from monitoramento m where m.fk_id_tipopessoa = 2 and m.fk_id_campus = c.id),0) as tottecnicos,
                coalesce((select count(m.id) as total from monitoramento m where m.fk_id_tipopessoa = 3 and m.fk_id_campus = c.id),0) as totestudantes,
                coalesce((select count(m.id) as total from monitoramento m where m.fk_id_tipopessoa = 4 and m.fk_id_campus = c.id),0) as totterceirizados,
                coalesce((select count(m.id) as total from monitoramento m where m.fk_id_tipopessoa = 5 and m.fk_id_campus = c.id),0) as totestagiarios,
                coalesce((select count(m.id) as total from monitoramento m where m.fk_id_campus = c.id),0) as total
                from campus c
                order by c.nome";

                $resultado = $conexao->consultar($query);
                foreach($resultado as $linha){
                    echo "<tr>";
                    echo "<td>{$linha["nome"]}</td>";
                    echo "<td>{$linha["totdocentes"]}</td>";
                    echo "<td>{$linha["tottecnicos"]}</td>";
                    echo "<td>{$linha["totestudantes"]}</td>";
                    echo "<td>{$linha["totterceirizados"]}</td>";
                    echo "<td>{$linha["totestagiarios"]}</td>";
                    echo "<td>{$linha["total"]}</td>";
                    echo "<td><a href='monitoramentocampusadm.php?cmp={$linha["id"]}'>Detalhar</a></td>";
                    echo "</tr>";
                }
            ?>
                </tbody>
        </div>

    </body>
</html>